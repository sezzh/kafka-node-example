const Koa = require('koa');
const https = require('https');
const http = require('http');
const Kafka = require('node-rdkafka');
const app = new Koa();
const JSONStream = require('streaming-json-stringify');

const topicConfig = {}

// logger

app.use(async (ctx, next) => {
    console.log("primer use")
    await next();
    const rt = ctx.response.get('X-Response-Time');
    console.log(`${ctx.method} ${ctx.url} - ${rt}`);
});

// x-response-time

app.use(async (ctx, next) => {
    console.log("segundo use")
    const start = Date.now();
    await next();
    const ms = Date.now() - start;
    ctx.set('X-Response-Time', `${ms}ms`);
});

// response

app.use(async ctx => {
    const kafkaConsumer = Kafka.createReadStream({
        'group.id': 'kafka',
        'metadata.broker.list': 'localhost:9092',
    }, topicConfig, {
        topics: ['Topic1']
    });

    let count = 0
    ctx.type = 'json';
    const stream = ctx.body = JSONStream();

    stream.on('error', ctx.onerror);

    kafkaConsumer.on('data', (message) => {
        stream.write({
            message: message.value.toString()
        });

        count++;
    })

});


https.createServer(app.callback()).listen(3001);
http.createServer(app.callback()).listen(3000);