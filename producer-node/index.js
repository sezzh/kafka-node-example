const Kafka = require('node-rdkafka');

var stream = Kafka.Producer.createWriteStream({
    'metadata.broker.list': 'localhost:9092'
}, {}, {
        topic: 'Topic1'
    });

let messageNumber = 1;

setInterval(() => {
    var queuedSuccess = stream.write(new Buffer('message in broke number: ' + messageNumber));
    messageNumber++;

    if (queuedSuccess) {
        console.log('We queued our message!');
    } else {
        // Note that this only tells us if the stream's queue is full,
        // it does NOT tell us if the message got to Kafka!  See below...
        console.log('Too many messages in our queue already');
    }

    stream.on('error', function (err) {
        // Here's where we'll know if something went wrong sending to Kafka
        console.error('Error in our kafka stream');
        console.error(err);
    })
}, 1000);

